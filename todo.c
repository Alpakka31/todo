#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>

/* function to create a file if it doesn't exist */
int create_file_if_not_exists(char* path) {
    if (access(path, F_OK) != 0) {
        printf("%s\n", "creating a new todolist");
        FILE* fp = fopen(path, "w");
        if (fp == NULL) {
            perror("failed to open file");
            return -1;
        }

        fclose(fp);
        return 0;
    }
    return 0;
}

/* function to count the total lines in a file */
int total_lines_in_file(char* path) {
    if (access(path, F_OK) == 0) {
        int count = 1;
        FILE* fp = fopen(path, "r");
        if (fp == NULL) {
            perror("failed to open file");
            return -1;
        }

        char ch = {0};
        while ((ch = fgetc(fp)) != EOF) {
            if (ch == '\n') { count++; }
        }

        fclose(fp);
        return count;
    } else {
        fprintf(stderr, "%s%s%s\n", "file '", path, "' doesn't exist");
        return -1;
    }
}

/* function to add a todo to a todolist */
int add_todo(char* path, char* input) {
    if (access(path, F_OK) == 0) {
        FILE* fp = fopen(path, "a");
        if (fp == NULL) {
            perror("failed to open file");
            return -1;
        }

        /* get total lines in a file */
        int lines = total_lines_in_file(path);
        if (lines == -1) { fclose(fp); return -1; }

        /* the line number will work as an id */
        fprintf(fp, "%d:%s\n", lines, input);
        fclose(fp);
        return 0;
    } else {
        fprintf(stderr, "%s%s%s\n", "file '", path, "' doesn't exist");
        return -1;
    }
}

/* function to list all the todos in the todolist */
int list_todo(char* path) {
    if (access(path, F_OK) == 0) {
        FILE* fp = fopen(path, "r");
        if (fp == NULL) {
            perror("failed to open file");
            return -1;
        }
        /* print all the todos from the todolist */
        char buf[1024] = {0};
        while (fgets(buf, sizeof(buf), fp) != NULL) {
            fprintf(stdout, "%s", buf);
        }

        fclose(fp);
        return 0;
    } else {
        fprintf(stderr, "%s%s%s\n", "file '", path, "' doesn't exist");
        return -1;
    }
}

/* remove a todo from a todolist by id */
int remove_todo(char* path, int rm_id) {
    if (access(path, F_OK) == 0) {
        FILE* fp = fopen(path, "r");
        if (fp == NULL) {
            perror("failed to open file");
            return -1;
        }

        /* create a path to a temporary file */
        char temp_file[256] = {0};
        snprintf(temp_file, sizeof(temp_file), "%s.temp", path);

        FILE* temp_fp = fopen(temp_file, "w");
        if (temp_fp == NULL) {
            perror("failed to open file");
            fclose(fp);
            return -1;
        }


        int id = 0, new_id = 0;;
        char buf[1024] = {0};
        char text[1024] = {0};
        bool found_id = false;

        while (fgets(buf, sizeof(buf), fp) != NULL) {
            /* extract the todo id and its text to separate variables */
            if (sscanf(buf, "%d:%1023[^\n]", &id, text) == 2) {
                /* tell user that the wanted todo will be removed if it was found */
                if (id == rm_id) {
                    printf("%s %d\n", "removing a todo with an id of:", rm_id);
                    found_id = true;
                }

                /* don't write the intended line that has to be removed to the temporary file */
                if (id != rm_id) {
                    /* write the all the todos to the temporary file in the correct order */
                    fprintf(temp_fp, "%d:%s\n", ++new_id, text);
                }
            } else {
                fprintf(stderr, "%s\n", "invalid line");
                fclose(fp);
                fclose(temp_fp);
                return -1;
            }
        }

        /* tell user if they try to remove a non-existent todo */
        if (found_id == false) {
            printf("%s '%d' %s\n", "a todo with an id of", rm_id, "wasn't found");
        }

        fclose(fp);
        fclose(temp_fp);

        /* remove the original file */
        if (remove(path) != 0) {
            perror("remove");
            return -1;
        }

        /* replace original file with the changes in the temporary file */
        if (rename(temp_file, path) != 0) {
            perror("rename");
            return -1;
        }
        return 0;
    } else {
        fprintf(stderr, "%s%s%s\n", "file '", path, "' doesn't exist");
        return -1;
    }
    
}

/* function to edit a todo */
int edit_todo(char* path, int edit_id, char* edit_str) {
    if (access(path, F_OK) == 0) {
        FILE* fp = fopen(path, "r");
        if (fp == NULL) {
            perror("failed to open file");
            return -1;
        }

        /* create a path to a temporary file */
        char temp_file[256] = {0};
        snprintf(temp_file, sizeof(temp_file), "%s.temp", path);

        FILE* temp_fp = fopen(temp_file, "w");
        if (temp_fp == NULL) {
            perror("failed to open file");
            fclose(fp);
            return -1;
        }

        int id = 0;
        char buf[1024] = {0};
        char text[1024] = {0};

        while (fgets(buf, sizeof(buf), fp) != NULL) {
            /* extract the id and the text of todo into separate variables */
            if (sscanf(buf, "%d:%1023[^\n]", &id, text) == 2) {
                /* if the ids match, then write then replace the original text
                 * with the edited one */
                if (id == edit_id) {
                    /* write with the edited text */
                    fprintf(temp_fp, "%d:%s\n", edit_id, edit_str);
                } else {
                    /* otherwise write the original text */
                    fprintf(temp_fp, "%s", buf);
                }
            } else {
                fprintf(stderr, "%s\n", "invalid line");
                fclose(fp);
                fclose(temp_fp);
                return -1;
            }
        }

        fclose(fp);
        fclose(temp_fp);

        /* remove the original file */ 
        if (remove(path) != 0) {
            perror("remove");
            return -1;
        }

        /* replace original file with the changes in the temporary file */
        if (rename(temp_file, path) != 0) {
            perror("rename");
            return -1;
        }
        return 0;
    } else {
        fprintf(stderr, "%s%s%s\n", "file '", path, "' doesn't exist");
        return -1;
    }
}

void usage(void) {
    printf("%s\n\n%s\n%s\n%s\n%s\n%s\n", "Usage: todo [add|rm|edit|list|usage]",
            "- add <text>",
            "- rm <id>",
            "- edit <id> <text>",
            "- list",
            "- usage");
}

/* function to check for numbers in a variable */
bool check_for_numbers(const char* str) {
    while (*str) {
        if (!isdigit((unsigned char)*str)) { return false; }
        str++;
    }
    return true;
}

int main(int argc, char** argv) {
    /* argument parser */
    if (argc <= 1) {
        usage();
        printf("%s\n", "not enough arguments");
        return EXIT_FAILURE;
    } else if (argc > 4) {
        usage();
        printf("%s\n", "too many arguments");
        return EXIT_FAILURE;
    }
    
    char* option = argv[1];

    /* get the current user's home directory */
    char* home = getenv("HOME");
    if (home == NULL) {
        printf("%s\n", "couldn't get home directory");
        return EXIT_FAILURE;
    }

    /* create a path to a todolist file */
    char* todolist_path = malloc(256 * sizeof(char));
    if (todolist_path == NULL) {
        perror("malloc");
        return EXIT_FAILURE;
    }
    memset(todolist_path, 0, 256);
    snprintf(todolist_path, 256, "%s/.todolist", home);
   
   /* create the todolist file if it doesn't already exist */ 
   int create_file = create_file_if_not_exists(todolist_path);
   if (create_file == -1) {
       free(todolist_path);
       todolist_path = 0;
       return EXIT_FAILURE;
   }

    /* there should be given a maximum of 4 arguments */
    if (argc > 1 && argc < 5) {
        /* checks for the add option */
        if (strncmp(option, "add", 3) == 0) {
            if (argc >= 4) {
                printf("%s\n", "too many arguments");
                free(todolist_path);
                todolist_path = 0;
                return EXIT_FAILURE;
            }

            /* there should be 3 arguments in total */
            if (argc == 3) {
                /* given text shouldn't be empty  */
                if (strlen(argv[2]) == 0) {
                    printf("%s\n", "no text given");
                    free(todolist_path);
                    todolist_path = 0;
                    return EXIT_FAILURE;
                }

                /* add a todo to a todolist */
                printf("%s%s%s\n", "adding '", argv[2], "' to the todolist");
                int ret = add_todo(todolist_path, argv[2]);
                if (ret == -1) {
                    free(todolist_path);
                    todolist_path = 0;
                    return EXIT_FAILURE;
                }
            } else {
                usage();
                printf("%s\n", "missing todo text");
                free(todolist_path);
                todolist_path = 0;
                return EXIT_FAILURE;
            }
        /* checks for remove option */
        } else if (strncmp(option, "rm", 2) == 0) {
            /* there shouldn't be more than or equal to 4 arguments */
            if (argc >= 4) {
                printf("%s\n", "too many arguments");
                free(todolist_path);
                todolist_path = 0;
                return EXIT_FAILURE;
            }

           /* given text shouldn't be empty */
           if (argc == 3) {
                if (strlen(argv[2]) == 0) {
                    printf("%s\n", "no text given");
                    free(todolist_path);
                    todolist_path = 0;
                    return EXIT_FAILURE;
                }

                /* validate the argument to not contain non-numeric characters */
                if (check_for_numbers(argv[2]) == true) {
                    int ret = remove_todo(todolist_path, atoi(argv[2]));
                    if (ret == -1) {
                        free(todolist_path);
                        todolist_path = 0;
                        return EXIT_FAILURE;
                    }
                } else {
                    printf("%s\n", "only numbers are allowed");
                    free(todolist_path);
                    todolist_path = 0;
                    return EXIT_FAILURE;
                }
           } else {
                usage();
                printf("%s\n", "missing todo id");
                free(todolist_path);
                todolist_path = 0;
                return EXIT_FAILURE;
           } 
        /* checks for the list option */
        } else if (strncmp(option, "list", 4) == 0) {
            /* no more than 2 arguments should be given */
            if (argc < 3) {
                printf("%s\n", "listing all todos in the todolist");
                int ret = list_todo(todolist_path);
                if (ret == -1) {
                    free(todolist_path);
                    todolist_path = 0;
                    return EXIT_FAILURE;
                }
            } else {
                usage();
                printf("%s\n", "too many arguments");
                free(todolist_path);
                todolist_path = 0;
                return EXIT_FAILURE;
            }
        /* checks for the edit option */
        } else if (strncmp(option, "edit", 4) == 0) {
            if (argc == 4) {
                /* validate the argument to not contain non-numeric characters */
                if (!check_for_numbers(argv[2])) {
                    printf("%s\n", "only numbers are allowed");
                    free(todolist_path);
                    todolist_path = 0;
                    return EXIT_FAILURE;
                }

                /* given text shouldn't be empty */
                if (strlen(argv[3]) == 0) {
                    printf("%s\n", "no text given");
                    free(todolist_path);
                    todolist_path = 0;
                    return EXIT_FAILURE;
                }

                printf("%s%d%s%s%s\n", "editing a todo with an id of '", atoi(argv[2]), "' to '", argv[3], "'");
                int ret = edit_todo(todolist_path, atoi(argv[2]), argv[3]);
                if (ret == -1) {
                    free(todolist_path);
                    todolist_path = 0;
                    return EXIT_FAILURE;
                }
            } else {
                usage();
                printf("%s\n", "missing todo id, todo text or both");
                free(todolist_path);
                todolist_path = 0;
                return EXIT_FAILURE;
            }
        /* checks for the usage option */
        } else if (strncmp(option, "usage", 5) == 0) {
            /* no more than 2 arguments should be given */
            if (argc < 3) {
                usage();
                free(todolist_path);
                todolist_path = 0;
            } else {
                usage();
                printf("%s\n", "too many arguments");
                free(todolist_path);
                todolist_path = 0;
                return EXIT_FAILURE;
            }
        } else {
            usage();
            printf("%s\n", "invalid option");
        }
    }
    

    free(todolist_path);
    todolist_path = 0;
    return EXIT_SUCCESS;
}
